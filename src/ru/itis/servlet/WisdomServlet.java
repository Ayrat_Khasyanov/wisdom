package ru.itis.servlet;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "test", urlPatterns = {"/wisdom", "/"})
public class WisdomServlet extends HttpServlet {
    private Configuration cfg;
    private static final String dbDriver = "org.apache.derby.jdbc.EmbeddedDriver";
    private static final String dbUrl = "jdbc:derby:";
    private static final String dbName = "Wisdom";
    private static final String dbTable = "Quotes";
    private static final String dbColumn = "quote";
    private static final String dbProps  = ";create=true;user=wise;password=unwise";
    private static Connection dbCon = null;
    private static String defaultQuote = "Нечего сказать";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String userWisdom = request.getParameter("user_wisdom");

        response.setContentType("text/html; charset=utf8");

//        HttpSession session = request.getSession();
//        ServletContext context = request.getServletContext();


        String wisdom = getQuote();

        try {
            PrintWriter out = response.getWriter();

            cfg.setServletContextForTemplateLoading(request.getServletContext(), "/ftl");

            Map<String, Object> wisdomMap = new HashMap<String, Object>();
            wisdomMap.put("wisdom", wisdom);

            Template temp = cfg.getTemplate("wisdom.ftl");
            temp.process(wisdomMap, out);

            out.close();
        } catch (TemplateException e) {
            e.printStackTrace();
        }

        if (userWisdom != null) {
            storeQuote(userWisdom);
        }
    }

    private void storeQuote(String userWisdom) {
        String insert = "INSERT INTO "+dbTable+"("+dbColumn+") VALUES(?)";

        try{
           PreparedStatement ps = dbCon.prepareStatement(insert);
           ps.setString(1,userWisdom);
           ps.execute();
           ps.close();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    private String getQuote(){
        String select = "SELECT " +dbColumn+", RANDOM() FROM "+dbTable+" ORDER BY 2 FETCH FIRST 1 ROWS ONLY";
        String quouteFound = null;
        PreparedStatement ps = null;
        try {
            ps = dbCon.prepareStatement(select);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                quouteFound = rs.getString(dbColumn);
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(quouteFound == null){
            quouteFound = defaultQuote;
        }
        return quouteFound;
    }

    @Override
    public void init() throws ServletException {
        super.init();
        initFreemarker();
        initDB();
    }

    private void initFreemarker(){
        cfg = new Configuration(Configuration.VERSION_2_3_26);
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
        cfg.setLogTemplateExceptions(false);
    }

    private void initDB(){
        try {
            Class.forName(dbDriver) ;
            dbCon = DriverManager.getConnection(dbUrl + dbName + dbProps);
            dbCon.setAutoCommit(false);
        } catch(Exception e) {
            e.printStackTrace();
        }

        DatabaseMetaData dbm = null;
        try {
            dbm = dbCon.getMetaData();
            ResultSet tables = dbm.getTables(null, null, dbTable, null);
            if (!tables.next()) {
                try {
                    PreparedStatement create = dbCon.prepareStatement("create table "+dbTable+"("+dbColumn+" varchar(255))");
                    create.executeUpdate();
                } catch (SQLException e) {
                     e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void destroy(){
        try
        {
            if (dbCon != null) {
                DriverManager.getConnection(dbUrl + ";shutdown=true");
                dbCon.close();
            }
        }
        catch (SQLException sqlExcept) {
                sqlExcept.printStackTrace();
        }
    }
}
